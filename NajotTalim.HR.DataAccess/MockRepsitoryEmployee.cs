﻿using System.Collections.Concurrent;

namespace NajotTalim.HR.DataAccess
{
    public  class MockRepsitoryEmployee : IEmployeeRepository
    {
        private static ConcurrentDictionary<int, Employee> _employees  = new ConcurrentDictionary<int, Employee>();
        private static object _employeesLock = new ();

        public MockRepsitoryEmployee()
        {
                Init();
        }
        private  void Init()
        {
            _employees.TryAdd(1, new Employee { Id  = 1, FullName = "Jonny English", Department = "IT" , Email= "jonny@gmail.com"});
            _employees.TryAdd(2, new Employee { Id  = 2, FullName = "Alina Denger", Department = "Model" , Email= "alina@gmail.com"});
            _employees.TryAdd(3, new Employee { Id  = 3, FullName = "Jessica Elfa", Department = "Artist" , Email= "jessica@gmail.com"});
            _employees.TryAdd(4, new Employee { Id  = 4, FullName = "Jonny Rodriguez", Department = "Enginer" , Email= "adrian@gmail.com"});
            _employees.TryAdd(5, new Employee { Id  = 5, FullName = "Barbarossa", Department = "Supermodel" , Email= "barbarossa@gmail.com"});
        }

        public  async Task <IEnumerable<Employee>>  GetEmployees() 
        {
            return await Task.FromResult(_employees.Values);
        }
        public  async Task<Employee>  GetEmployee(int id) 
        {
            return await Task.FromResult( _employees[id]);
        }

        public async Task<Employee> CreateEmployee(Employee employee)
        {
            int NewId = 0;
           lock (_employeesLock)
            {
                NewId = _employees.Keys.Max() + 1;
            }
           employee.Id = NewId;
            _employees.TryAdd(NewId, employee);
            return await Task.FromResult(employee);
        }

        public async Task<Employee> UpdateEmployee(int id, Employee employee)
        {
          await Task.FromResult(_employees[id] = employee);
            return employee;
        }

        public Task<bool> DeleteEmploye(int id)
        {
            if (_employees.ContainsKey(id))
            {
                _employees.TryRemove(id, out _);
                return Task.FromResult(true);
            }
            else 
                return Task.FromResult(false);
        }

        public Task UpdateEmployee(Employee employee)
        {
            throw new NotImplementedException();
        }
    }
}