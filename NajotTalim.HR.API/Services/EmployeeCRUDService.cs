﻿using NajotTalim.HR.API.Models;
using NajotTalim.HR.DataAccess;

namespace NajotTalim.HR.API.Services
{
    public class EmployeeCRUDService : IGenericCRUDService<EmployeeModel>
    {
        private readonly IEmployeeRepository _employeeReository;
        public EmployeeCRUDService(IEmployeeRepository employeeReository)
        {
            _employeeReository = employeeReository;
        }
         public  async Task<EmployeeModel> Create(EmployeeModel model)
        {
            var employee = new Employee
            {
                FullName = model.FullName,
                Department = model.Department,
                Email = model.Email,
  
            };
            var CreatedEmployee = await _employeeReository.CreateEmployee(employee);
            var result = new EmployeeModel
            {
                FullName = CreatedEmployee.FullName,
                Department = CreatedEmployee.Department,
                Email = CreatedEmployee.Email,
                Id = CreatedEmployee.Id,
            };
            return result;
        }

        public async Task<bool> Delete(int id)
        {
            return await _employeeReository.DeleteEmploye(id);
        }

        public async Task<EmployeeModel> Get(int id)
        {
         var employee =  await  _employeeReository.GetEmployee(id);
            var model = new EmployeeModel
            {
                Id = employee.Id,
                FullName = employee.FullName,
                Department = employee.Department,
                Email = employee.Email
            };
            return model;
        }

        public async Task<IEnumerable<EmployeeModel>> GetAll()
        {
            var result = new List<EmployeeModel>();
            var employees = await _employeeReository.GetEmployees();
            foreach (var employee in employees)
            {
                var model = new EmployeeModel
                {
                    FullName = employee.FullName,
                    Department = employee.Department,
                    Email = employee.Email,
                    Id = employee.Id,
                };
                result.Add(model);
            };
            return result;
        }

        public async Task<EmployeeModel> Update(int id, EmployeeModel model)
        {
            var employee = new Employee
            {
                FullName = model.FullName,
                Department = model.Department,
                Email = model.Email,
                Id = model.Id,

            };
            var updatedEmployee = await _employeeReository.UpdateEmployee(id, employee);
            var result = new EmployeeModel
            {
                FullName = updatedEmployee.FullName,
                Department = updatedEmployee.Department,
                Email = updatedEmployee.Email,
                Id = updatedEmployee.Id,
            };
            return result;
        }

        Task<EmployeeModel> IGenericCRUDService<EmployeeModel>.Create(EmployeeModel employee)
        {
            throw new NotImplementedException();
        }
    }
}
